#include <stdio.h>
#include <string.h>
#include "mem_internals.h"
#include "mem.h"
#include "mem_debug.h"

#define VARS_COUNT 5


void init_test(char* const header, uint8_t* vars[], size_t const vars_count, size_t const mem) {
  static size_t test_num = 1;
  printf("**************** Test %zu ****************\n%s\n", test_num, header);

  for (size_t i = 0; i < vars_count; i++) {
    vars[i] = (uint8_t *)_malloc(mem);
    for (size_t j = 0; j < mem; j++) {
        vars[i][j] = (uint8_t)255;
    }
  }


  test_num++;
}


int main() {

  void* heap = heap_init(20000);
  uint8_t* vars[VARS_COUNT];

  init_test("Normal successful memory allocation", vars, VARS_COUNT, 50);
  debug_heap(stdout, heap);
  printf("\n");
  free_heap(heap);

  init_test("Freeing one block from several allocated", vars, VARS_COUNT, 50);
  _free(vars[4]);
  debug_heap(stdout, heap);
  printf("\n");
  free_heap(heap);

  init_test("Freeing two blocks from several allocated ones", vars, VARS_COUNT, 50);
  _free(vars[4]);
  _free(vars[3]);
  debug_heap(stdout, heap);
  printf("\n");
  free_heap(heap);

  init_test("The memory is over, the new region of memory\n expands the old", vars, VARS_COUNT, 4079); // 4 Kb
  debug_heap(stdout, heap);
  printf("\n");
  free_heap(heap);

  init_test("The memory has run out, the old memory\n"
            "region cannot be expanded due to\n"
            "a different allocated range of addresses,\n"
            "the new region is allocated in a different place.", vars, VARS_COUNT, 134217711); // 128 Mb
  debug_heap(stdout, heap);
  printf("\n");
  free_heap(heap);


  return 0;
}
